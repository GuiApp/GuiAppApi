<?php
/**
 * Description of CuentaAPI
 *
 * @author meza
 */
class CuentaAPI extends EntityAPI {
    const API_ACTION = 'cuenta';
    
    public function __construct() {
	$this->db = new CuentaDB();
        $this->fields = [];
        array_push($this->fields, 
            'id', 
            'nombre', 
            'email', 
            'provider', 
            'notificaciones');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        if($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert($obj->id, $obj->nombre, $obj->email, 
                $obj->provider, $obj->notificaciones);
        
        if($r) {$this->response(200,"success","new record added"); }
        else { $this->response(204,"success","record duplicated"); }
    }
    
    function processPut() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        } 
        $r = $this->db->update($id,
                $obj->nombre, $obj->email, 
                $obj->provider, $obj->notificaciones);
        
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}