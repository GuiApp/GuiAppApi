<?php
/**
 * Description of PublicidadDB
 *
 * @author meza
 */
class PublicidadDB extends EntityDB{
   protected $mysqli;
   const TABLE = 'publicidades';
    
    public function getById($id=0){
        $query = "SELECT p.id, p.idsponsor, s.razonsocial, p.fecfin, p.fecinicio,
                    p.imagen, p.url 
                FROM publicidades p 
                LEFT JOIN sponsors s ON p.idsponsor = s.id
                WHERE p.id=$id;";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT p.id, p.idsponsor, s.razonsocial, p.fecfin, p.fecinicio,
                    p.imagen, p.url 
                FROM publicidades p 
                LEFT JOIN sponsors s ON p.idsponsor = s.id;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getRandom(){
        $query = "SELECT * FROM publicidades "
                . "WHERE fecinicio < NOW() AND Fecfin >= NOW() "
                . "ORDER BY rand() limit 1";
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert( $idsponsor=-1, 
        $fecinicio='', $fecfin='', $imagen='', $url=''){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " (idsponsor, "
                . "fecinicio, fecfin, imagen, url) "
                . "VALUES ($idsponsor, '$fecinicio', '$fecfin', '$imagen', '$url');");
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, $idsponsor=-1, 
            $fecinicio='', $fecfin='', $imagen='', $url='') {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET idsponsor=?, "
                    . "fecinicio=?, fecfin=?, imagen=?, url=? "
                    . "WHERE id = ?;");
            $stmt->bind_param('issssi', $idsponsor, $fecinicio, $fecfin, 
                    $imagen, $url, $id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
   
    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}