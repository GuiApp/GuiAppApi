<?php
/**
 * Description of SucursalAPI
 *
 * @author meza
 */
class SucursalAPI extends EntityAPI {
    const API_ACTION = 'sucursal';
    const GET_POR_EMPRESA = 'porempresa';
    const GET_BYID = 'byid';
	
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
        $this->db = new SucursalDB();

        switch ($method) {
            case 'GET'://consulta
                $this->processGet();
                break;
            case 'POST'://inserta
                $this->processPost();
                break;
            case 'PUT'://actualiza
                $this->processPut();
                break;
            case 'DELETE'://elimina
                $this->proceesDelete();
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }
	
    function processGet() {
        $id = filter_input(INPUT_GET, 'id');
        
        if ($id) {
            $isXEmpresa = strpos($id, self::GET_POR_EMPRESA);
            $isById = strpos($id, self::GET_BYID);
            
            if ($isXEmpresa !== false) {
                $fld1 = filter_input(INPUT_GET, 'fld1');
                $response = $this->db->getListXEmpresa($fld1);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } elseif ($isById !== false) {
                $response = $this->db->getById($_GET['fld1']);
                echo json_encode($response, JSON_PRETTY_PRINT);
            }
        } else {
            $response = $this->db->getList();
            echo json_encode($response, JSON_PRETTY_PRINT);
        }
    }
//No acepta el cmpo "24hs"	
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        } 
        /*
	"id": "e71ba9aa-136c-49a4-a448-9d1942080383",
	"palabrasclave": null,*/
        if(isset($obj->id) AND isset($obj->idempresa) AND isset($obj->direccion) AND 
                isset($obj->telefono) AND isset($obj->latitud) AND isset($obj->longitud) AND 
                isset($obj->delivery) AND isset($obj->veinticuatrohs) AND isset($obj->diashorarios) AND 
                isset($obj->dirty)) {
            $r = $this->db->insert($obj->id, $obj->idempresa, $obj->direccion, 
                    $obj->telefono, $obj->latitud, $obj->longitud, 
                    $obj->delivery, $obj->veinticuatrohs, $obj->diashorarios, 
                    0);
            if($r) { $this->response(200,"success","new record added"); }
        } else {
            $this->response(422,"error","The property is not defined");
        }
    }
    
    function processPut() {
        if(isset($_GET['id'])){
            $obj = json_decode( file_get_contents('php://input') );   
            $objArr = (array)$obj;
            if (empty($objArr)){                        
                $this->response(422,"error","Nothing to add. Check json");                        
            }else if(isset($obj->idempresa) AND isset($obj->direccion) AND 
                    isset($obj->latitud) AND isset($obj->longitud) AND 
                    isset($obj->diashorarios) AND isset($obj->veinticuatrohs) AND 
                    isset($obj->delivery) AND isset($obj->telefono) and 
                    isset($obj->dirty)) {
                
                $r = $this->db->update($_GET['id'], 
                    $obj->idempresa, $obj->direccion,
                    $obj->latitud, $obj->longitud, 
                    $obj->diashorarios, $obj->veinticuatrohs,
                    $obj->delivery, $obj->telefono, 
                    $obj->dirty);
                if($r)
                    $this->response(200,"success","Record updated");                             
                else
                    $this->response(304,"success","Record not updated");                             
            }else{
                $this->response(422,"error","The property is not defined");                        
            }     
            exit;
        }
        $this->response(400);
    }
      function proceesDelete(){
        if( isset($_GET['action']) && isset($_GET['id']) ){
            if($_GET['action']==self::API_ACTION){  
                $this->db->delete($_GET['id']);
                $this->response(204);                   
                exit;
            }
        }
        $this->response(400);
    }
    }