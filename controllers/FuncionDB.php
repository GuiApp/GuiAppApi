<?php
/**
 * Description of FuncionDB
 *
 * @author meza
 */
class FuncionDB extends EntityDB{
   protected $mysqli;
   const TABLE = 'funciones';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT f.id, f.descripcion, f.cantidad  "
              . "FROM `funciones` f ");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdCuenta($idcuenta=''){
        $query = "SELECT c.id, 'funcion' AS funcion, SUM(c.cantidad) AS cantidad, 
                m.id AS idmodulo, m.modulo AS modulo,
                IFNULL((CASE m.id 
                    WHEN 1 THEN 
                        IFNULL((SELECT COUNT(p1.id)
                        FROM publicaciones p1 
                        LEFT JOIN empresas e1 ON p1.idempresa = e1.id 
                        WHERE e1.idcuenta = '$idcuenta' AND p1.destacada = 0), 0)  >= SUM(c.cantidad)
                    WHEN 2 THEN 
                        IFNULL((SELECT COUNT(e2.id)
                        FROM empresas e2 
                        WHERE e2.idcuenta = '$idcuenta'), 0) >= SUM(c.cantidad)
                    WHEN 3 THEN 
                        IFNULL((SELECT COUNT(s3.id)
                        FROM sucursales s3 
                        LEFT JOIN empresas e3 ON e3.id = s3.idempresa
                        WHERE e3.idcuenta = '$idcuenta'), 0) >= SUM(c.cantidad)
                    WHEN 4 THEN
                        IFNULL((SELECT COUNT(v4.id)
                        FROM eventos v4 
                        LEFT JOIN empresas e4 ON e4.id = v4.idempresa
                        WHERE e4.idcuenta = '$idcuenta'), 0) >= SUM(c.cantidad)
                    WHEN 5 THEN
                        IFNULL((SELECT COUNT(p5.id)
                        FROM publicaciones p5 
                        LEFT JOIN empresas e5 ON e5.id = p5.idempresa
                        WHERE e5.idcuenta = '$idcuenta' AND p5.destacada = 1), 0) >= SUM(c.cantidad)
                END), 1) AS limalcanzado
            FROM modulos m
            LEFT JOIN 
                (SELECT f.id, f.cantidad, f.idmodulo, p.idcuenta  
                FROM funciones f
                LEFT JOIN funcionesxpaquete fx ON fx.idfuncion = f.id
                LEFT JOIN paquetesxcuenta p ON p.idpaquete = fx.idpaquete
                LEFT JOIN modulos m ON m.id = f.idmodulo) c ON c.idmodulo = m.id
            WHERE c.idcuenta = '$idcuenta'
            GROUP BY m.id;";
//        var_dump($query);
//        return true;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($descripcion='', $cantidad=-1, $modulo){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " "
                . "(descripcion, cantidad, modulo) "
                . "VALUES ('$descripcion', $cantidad, $modulo);");
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, $descripcion='', $cantidad=-1, $modulo) {
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " "
                    . "SET descripcion = '$descripcion', cantidad = $cantidad, modulo = $modulo"
                    . "WHERE id = $id;");
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}
