<?php
/**
 * Description of VidrieraVirtualAPI
 *
 * @author meza
 */
class ImagenAPI extends EntityAPI {
    const API_ACTION = 'imagen';
    const GET_BYIDEMPRESA = 'porempresa';
    
    public function __construct() {
	$this->db = new ImagenDB();
        $this->fields = [];
        array_push($this->fields, 
            'idempresa',
            'url',
            'imagen');
    }
	
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        if($id){
            $isByIdEmpresa = strpos($id, self::GET_BYIDEMPRESA);
            if($isByIdEmpresa === 0) {
                $idempresa = filter_input(INPUT_GET, 'fld1');
                $response = $this->db->getByIdEmpresa($idempresa);
                echo json_encode($response,JSON_PRETTY_PRINT);
            } else {            
                $response = $this->db->getById($id);
                echo json_encode($response,JSON_PRETTY_PRINT);
            }
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
	
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert($obj->idempresa, $obj->url, $obj->imagen);
        
        if($r) {$this->response(200,"success","new record added"); }
        else { $this->response(204,"success","record duplicated"); }
    }
	
    function processPut() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id, 
                $obj->idempresa, $obj->url, $obj->imagen);
        
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}