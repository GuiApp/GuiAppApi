<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LocalizacionDB
 *
 * @author meza
 */
class LocalizacionDB extends EntityDB{
    protected $mysqli;
    const TABLE_PROV = 'vw_geo_provincias';
    const TABLE_LOC = 'vw_geo_localidades';
    const TABLE_MUNIC = 'vw_geo_municipalidades';

    public function getPaisList() {
        $stmt = $this->mysqli->prepare("SELECT * FROM "
                . self::TABLE_PROV . " WHERE country_code=?;");
        $stmt->bind_param('s', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }

    public function getProvListByPais($id = '') {
        $stmt = $this->mysqli->prepare("SELECT * FROM "
                . self::TABLE_PROV . " WHERE country_code=?;");
        $stmt->bind_param('s', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }

    public function getLocListByProv($id = '') {
        $stmt = $this->mysqli->prepare("SELECT * FROM "
                . self::TABLE_LOC . " WHERE admin1_code=?;");
        $stmt->bind_param('s', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }

    public function getMunicListByLoc($id = '') {
        $stmt = $this->mysqli->prepare("SELECT * FROM "
                . self::TABLE_MUNIC . " WHERE admin2_code=?;");
        $stmt->bind_param('s', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }

}
