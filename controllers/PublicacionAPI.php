<?php
/**
 * Description of PublicacionAPI
 *
 * @author meza
 */
class PublicacionAPI extends EntityAPI {
    const API_ACTION = 'publicacion';
    const GET_DESTACADAS = 'destacadas';
    const GET_PUBLICACIONES = 'regulares';
    const GET_LISTBACKOFFICE = 'listabackoffice';
    const GET_BYID = 'byid';
    const POST_FROMMOBILE = 'frommobile';
    const POST_IMAGEN = 'imagen';
    const PUT_FROMMOBILE = 'frommobile';
    const PUT_IMAGEN = 'imagen';
    const GET_BYIDCUENTA = 'idcuenta';
    const GET_BYIDEMPRESA = 'idempresa';
    
    public function __construct() {
	$this->db = new PublicacionDB();
        $this->fields = [];
//        array_push($this->fields, 
//            'id', 
//            'nombre', 
//            'email', 
//            'provider', 
//            'notificaciones');
    }

    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        if($id) {
            $isDestacadas = isset($id) ? $id === self::GET_DESTACADAS : false;//strpos($id, self::PREFIX_DESTACADAS);
            $isPublicaciones = isset($id) ? $id === self::GET_PUBLICACIONES : false;//strpos($id, self::PREFIX_PUBLICACIONES);
            $isBackoffice = isset($id) ? $id === self::GET_LISTBACKOFFICE : false;//strpos($id, self::GET_LISTBACKOFFICE);
            $isById = isset($id) ? $id === self::GET_BYID : false;//strpos($id, self::GET_BYID);
            $isByIdCuenta = isset($id) ? $id === self::GET_BYIDCUENTA : false;//strpos($id, self::GET_BYIDCUENTA);
            $isByIdEmpresa = isset($id) ? $id === self::GET_BYIDEMPRESA : false;//strpos($id, self::GET_BYIDEMPRESA);
            
            if ($isDestacadas !== false) {
                $response = $this->db->getListDests();//Order by Cronologicamente
                echo json_encode($response,JSON_PRETTY_PRINT);
            } elseif ($isPublicaciones !== false) {
                $response = $this->db->getListPubs();//Order by Cronologicamente
                echo json_encode($response,JSON_PRETTY_PRINT);
            } elseif ($isBackoffice !== false) {
                $response = $this->db->getList();
                echo json_encode($response,JSON_PRETTY_PRINT);
            } elseif ($isById !== false){
                $response = $this->db->getById($_GET['fld1']);
                echo json_encode($response,JSON_PRETTY_PRINT);                    
            }elseif ($isByIdCuenta !== false){
                $response = $this->db->listByIdCuenta($_GET['fld1']);
                echo json_encode($response,JSON_PRETTY_PRINT);                    
            }elseif ($isByIdEmpresa !== false){
                $response = $this->db->listByIdEmpresa($_GET['fld1']);
                echo json_encode($response,JSON_PRETTY_PRINT);                    
            }
        }
        else
            $this->response(400);
    }
	
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        $id = filter_input(INPUT_GET, 'id');
        
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        
        $isFromMobile = isset($id) ? $id === self::POST_FROMMOBILE : false;
        $isImagen = strpos($id, self::POST_IMAGEN);
        
        if($isFromMobile) {
            $dirty = 0;
            $r = $this->db->insertMovil($obj->id, $obj->titulo, $obj->imagen,
                    $obj->descripcion, $obj->idempresa, $dirty,
                    $obj->destacada);
            if($r) {
                $this->response(200,"success","new record added");
            } else {
                $this->response(422,"error","No se pudo guardar");
            }
        } elseif($isImagen !== false) {
            $this->postImage();
        } elseif (isset($obj->titulo) AND isset($obj->idempresa) 
                AND isset($obj->descripcion) AND isset($obj->imagen)) {
            $dirty = 0;
            $r = $this->db->insert($obj->id, $obj->titulo, $obj->imagen,
                    $obj->descripcion, $obj->idempresa, $dirty,
                    $obj->destacada);
            if($r)
                $this->response(200,"success","new record added");
        } else {
            $this->response(422,"error","The property is not defined");
        }        
    }
    
    function postImage() {
        $obj = json_decode(file_get_contents('php://input'));
        $r = $this->db->insertImage($obj->id, $obj->image);
        if ($r) {
            $this->response(200, "success", "new record added");
        }
    }
		
    function processPut() {
        $obj = json_decode( file_get_contents('php://input') );   
        $objArr = (array)$obj;
        $id = filter_input(INPUT_GET, 'id');

        if (empty($objArr)){                        
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        
        $isFromMobile = isset($id) ? $id === self::PUT_FROMMOBILE : false;
        $isImagen = strpos($id, self::PUT_IMAGEN);
        if($isFromMobile) {
            $dirty = 0;
            $r = $this->db->updateMovil($obj->id, $obj->idempresa, $obj->titulo, 
                    $obj->imagen, $obj->descripcion, $obj->destacada, 
                    $dirty);
            if($r) {
                $this->response(200,"success","record updated");
            } else {
                $this->response(422,"error","No se pudo guardar");
            }
        } elseif($isImagen !== false) {
            $this->putImage();
        } elseif(isset($obj->id) AND isset($obj->idempresa) 
                AND isset($obj->titulo) AND isset($obj->imagen) 
                AND isset($obj->descripcion) AND isset($obj->destacada)
                AND isset($obj->dirty)){
            $r = $this->db->update($_GET['id'], $obj->idempresa, $obj->titulo, 
                $obj->imagen, $obj->descripcion, $obj->destacada, 
                    $obj->dirty);
            //var_dump($r);
            if($r) {
                $this->response(200,"success","Record updated");
            } else {
                $this->response(304,"success","Record not updated");
            }
        }else{
            $this->response(422,"error","The property is not defined");                        
        }
    }
    
    function putImage() {
        $obj = json_decode(file_get_contents('php://input'));
        $r = $this->db->updateImage($obj->id, $obj->image);
        if ($r) {
            $this->response(200, "success", "new record added");
        }
    }
    
    function processDelete(){
        if(isset($_GET['id']) ){ 
                $this->db->delete($_GET['id']);
                $this->response(204);                   
                exit;
        }
        $this->response(400);
    }
 }