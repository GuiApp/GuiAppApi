<?php
/**
 * Description of SponsorDB
 *
 * @author meza
 */
class SponsorDB extends EntityDB{
   protected $mysqli;
   const TABLE = 'sponsors';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
	
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT * FROM ". self::TABLE);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
	
    public function insert($razonsocial=""){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " (razonsocial) "
                . "VALUES (?);");
        $stmt->bind_param('s', $razonsocial);
        $r = $stmt->execute();

        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, $razonsocial="") {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET razonsocial=? "
                    . "WHERE id = ?;");
            $stmt->bind_param('si', $razonsocial,$id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
   
    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
	
    public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
} 
