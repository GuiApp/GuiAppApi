<?php
/**
 * Description of EmpresaDB
 *
 * @author meza
 */
class EmpresaDB extends EntityDB{
    protected $mysqli;

    const TABLE = 'empresas';
    const TABLE_CXE = 'categoriasxempresas';

    public function getById($id = '') {
        $query = "SELECT * FROM " . self::TABLE . " WHERE id='$id';";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getEmpresasByCuentas($idcuenta) {
        $query = "SELECT "
                . "e.id, e.razonsocial, e.cuit, e.mediodepago, "
                . "e.estado, e.logo, e.facebook, e.twitter, "
                . "e.instagram, e.palabrasclave, e.dirty, e.web, "
                . "e.descripcion, e.idcuenta "
                . "FROM empresas e  "
                . "LEFT JOIN cuentas c ON e.idcuenta = c.id "
                . "WHERE c.id = '" . $idcuenta . "'";
        //var_dump($query);
        
        $result = $this->mysqli->query($query);
		$entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
     public function getDetalleByIdSuc($idsucursal) {
        $result = $this->mysqli->query(
		"SELECT "
                . "s.id, s.idempresa, e.razonsocial, e.logo, "
                . "e.estado, e.cuit, s.direccion, s.telefono, "
                . "e.web, s.diashorarios, e.mediodepago, e.descripcion, "
                . "e.facebook, e.instagram, e.twitter, e.idcuenta "
                . "FROM sucursales s "
                . "LEFT JOIN empresas e ON s.idempresa = e.id"
                . " WHERE s.id = " . $idsucursal);
		$entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getList() {
        $query="SELECT "
                . "e.id, e.razonsocial, e.cuit, e.mediodepago, "
                . "e.estado, e.logo, e.facebook, e.twitter, "
                . "e.instagram, e.palabrasclave, e.dirty, e.web, "
                . "e.descripcion, e.idcuenta "
                . "FROM `empresas` e ";
        $result = $this->mysqli->query($query);                
        //var_dump($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getCategorias($idcuenta) {
        $query = "SELECT * 
                FROM `categoriasxempresas` 
                WHERE idempresa = '$idcuenta'";
//        var_dump($query);
        
        $result = $this->mysqli->query($query);
		$entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function busqueda($param, $latitud, $longitud) {
        $param_process = trim($param);
        
        $words = '';
        $param_process = explode(' ', $param_process);
        $keyCount = 0;
        foreach ($param_process as $keys) { 
            if ($keyCount > 0){
                $words .= " AND";
            }
            $words .= "(r.palabrasclave LIKE '%$keys%') OR (r.razonsocial LIKE '%$keys%') "; 
            ++$keyCount;
        }
        
        $query = "SELECT * FROM ( 
                SELECT s.id, s.idempresa, e.razonsocial, e.estado, s.direccion, s.idpais, s.idprovincia, 
                s.idlocalidad, s.telefono, s.delivery, s.veinticuatrohs, 
                s.diashorarios, s.dirty, e.palabrasclave, 
                (3959 * ACOS(COS(RADIANS($latitud)) * COS(RADIANS(s.latitud)) * 
                COS(RADIANS( s.longitud) - RADIANS($longitud)) + SIN(RADIANS($latitud)) * 
                SIN(RADIANS(s.latitud)))) AS distancia 
                FROM sucursales s 
                LEFT JOIN empresas e ON s.idempresa = e.id 
                ORDER BY distancia) r 
                WHERE $words  
                LIMIT 50";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);

        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function xcategoria($idcategoria, $param, $latitud, $longitud) {
        $param_process = trim($param);
        
        $words = '';
        $param_process = explode(' ', $param_process);
        $keyCount = 0;
        foreach ($param_process as $keys) { 
            if ($keyCount > 0){
                $words .= " AND";
            }
            $words .= "(r.palabrasclave LIKE '%$keys%') OR (r.razonsocial LIKE '%$keys%') "; 
            ++$keyCount;
        }
        $query = "SELECT * FROM ( "
                . "SELECT s.id, s.idempresa, e.razonsocial, e.estado, cxe.idcategoria, s.direccion, s.idpais, s.idprovincia, "
                . "s.idlocalidad, s.telefono, s.delivery, s.veinticuatrohs, "
                . "s.diashorarios, s.dirty, e.palabrasclave, "
                . "(3959 * ACOS(COS(RADIANS($latitud)) * COS(RADIANS(s.latitud)) * "
                . "COS(RADIANS( s.longitud) - RADIANS($longitud)) + SIN(RADIANS($latitud)) * "
                . "SIN(RADIANS(s.latitud)))) AS distancia "
                . "FROM sucursales s "
                . "LEFT JOIN empresas e ON s.idempresa = e.id "
                . "LEFT JOIN categoriasxempresas cxe ON cxe.idempresa = e.id "
                . "ORDER BY distancia) r "
                . "WHERE r.idcategoria = $idcategoria AND ($words) "
                . "LIMIT 100";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }

    //FALTA EL LOGO
    public function insert($id='', $razonsocial = '', $cuit = -1, 
            $mediodepago = '', $estado = '', $logo = '', $facebook = '', 
            $twitter = '', $instagram = '', $palabraclave = '', 
            $web= '', $dirty=-1, $idcuenta=-1) {
        $query= "INSERT INTO " . self::TABLE . " (id, razonsocial, "
                . "cuit, mediodepago, estado, logo, facebook, twitter, instagram, "
                . "palabrasclave, web, dirty, idcuenta) "
                . "VALUES ('$id','$razonsocial', $cuit, '$mediodepago', '$estado', '$logo', "
                . "'$facebook', '$twitter', '$instagram', '$palabraclave', '$web', "
                . "$dirty, '$idcuenta');";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
        
    /*
     * Usos:
     * - Móvil
     */
    public function insertFromMovil(
            $id='', $razonsocial = '', $cuit = -1, 
            $mediodepago = '', $estado = '', $logo = '', 
            $facebook = '', $twitter = '', $instagram = '', 
            $palabraclave = '', $web= '', $dirty=-1, 
            $idcuenta=-1, $categorias= ''){
        
        $file = $_SERVER['DOCUMENT_ROOT'] . "/" . $this->conf['imagestock'] . "/e_$id.png";
        file_put_contents($file, base64_decode($logo));
        $logo = $this->conf['protocol'] . $_SERVER['SERVER_NAME'] . "/" . $this->conf['imagestock'] . "/e_$id.png";
        
        $query = "INSERT INTO empresas (
                id, razonsocial, cuit, 
                mediodepago, estado, logo, 
                facebook, twitter, instagram, 
                palabrasclave, web, dirty, 
                idcuenta) 
            VALUES (
                '$id','$razonsocial', $cuit, 
                '$mediodepago', '$estado', '$logo', 
                '$facebook', '$twitter', '$instagram',
                '$palabraclave', '$web', $dirty, 
                '$idcuenta');";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        $this->insertCategorias($id, $categorias);
        return $r;
    }
    
    protected function insertCategorias($id='', $categorias='') {
        $query = "DELETE FROM categoriasxempresas WHERE idempresa = '$id'";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $stmt->close();
        $query = "INSERT INTO categoriasxempresas 
                (idempresa, idcategoria)
            SELECT '$id', id FROM categorias WHERE id IN ('$categorias');";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
    }
    
    public function updateFromMovil(
            $id='', $razonsocial = '', $cuit = -1, 
            $mediodepago = '', $estado = '', $logo = '', 
            $facebook = '', $twitter = '', $instagram = '', 
            $palabrasclave = '', $web= '', $dirty=-1, 
            $idcuenta=-1, $categorias= ''){
            
        $pos = strrpos($logo, $_SERVER['DOCUMENT_ROOT'] . "/" . $this->conf['imagestock']);
        if ($pos === false) { // nota: tres signos de igual
            $file = $_SERVER['DOCUMENT_ROOT'] . "/" . $this->conf['imagestock'] . "/e_$id.png";
            file_put_contents($file, base64_decode($logo));
            $logo = $this->conf['protocol'] . $_SERVER['SERVER_NAME'] . "/" . $this->conf['imagestock'] . "/e_$id.png";
        }
        
        if ($this->checkID($id, self::TABLE)) {
            $query = "UPDATE " . self::TABLE . " SET 
                    razonsocial='$razonsocial', cuit=$cuit, 
                    mediodepago='$mediodepago', estado='$estado', logo='$logo', 
                    facebook='$facebook', twitter='$twitter', instagram='$instagram', 
                    palabrasclave='$palabrasclave', web='$web', dirty=$dirty, 
                    idcuenta='$idcuenta' 
                WHERE id = '$id';";
//            var_dump($query);
//            return true;
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute();
            $stmt->close();
            
            $this->insertCategorias($id, $categorias);
            return $r;
        }
        return false;
    }
    
    public function insertCXE($idempresa= '', $idcategoria=-1){
        $stmt = $this->mysqli->prepare( 
                "INSERT INTO " . self::TABLE_CXE . " (idempresa, idcategoria) VALUES (?, ?)"); 
        $stmt->bind_param('si', $idempresa, $idcategoria);
        $r = $stmt->execute();

        $stmt->close();
        return $r;
    }

    public function insertImage($id, $imagen) {
        $ini = parse_ini_file('conf.ini', true);
        
        $file = $_SERVER['DOCUMENT_ROOT'] . "/" . $this->conf['imagestock'] . "/e_$id.png";
        file_put_contents($file, base64_decode($imagen));
        
        $file = $this->conf['protocol'] . $_SERVER['SERVER_NAME'] . "/" . $this->conf['imagestock'] . "/e_$id.png";
        $query = "UPDATE " . self::TABLE . " SET logo='$file' "
                    . "WHERE id = '$id';";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        return $r;
    }

    public function delete($id = 0) {
        $stmt = $this->mysqli->prepare("DELETE FROM " . self::TABLE . " WHERE id = ?;");
        $stmt->bind_param('s', $id);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }

    public function update($id = '', $razonsocial = '', $cuit = -1, 
            $mediodepago = '', $estado = '', $logo = '',  $facebook = '', 
            $twitter = '', $instagram = '', $palabrasclave = '', $web='',  
            $dirty = -1, $idcuenta=-1) {
        if ($this->checkID($id, self::TABLE)) {
            $query = "UPDATE " . self::TABLE . " SET razonsocial='$razonsocial', "
                    . "cuit=$cuit, mediodepago='$mediodepago', estado='$estado', "
                    . "logo='$logo', facebook='$facebook', "
                    . "twitter='$twitter', instagram='$instagram', palabrasclave='$palabrasclave', "
                    . "web='$web', dirty=$dirty, idcuenta='$idcuenta' "
                    . "WHERE id = '$id';";
            //var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute();
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function updateCXE($idempresa = '', $idcategoria=-1) {
        $query = "DELETE FROM categoriasxempresas WHERE idempresa = '$idempresa';";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        
        return $this->insertCXE($idempresa, $idcategoria);
    }

    public function updateImage($id, $imagen) {
        $ini = parse_ini_file('conf.ini');
        
        $file = $_SERVER['DOCUMENT_ROOT'] . "/" . $this->conf['imagestock'] . "/e_$id.png";
        file_put_contents($file, base64_decode($imagen));
        
        $file = $this->conf['protocol'] . $_SERVER['SERVER_NAME'] . "/" . $this->conf['imagestock'] . "/e_$id.png";
        $query = "UPDATE " . self::TABLE . " SET logo='$file' "
                    . "WHERE id = '$id';";
        //var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        return $r;
    }

    public function checkID($id, $table) {
        $stmt = $this->mysqli->prepare("SELECT * FROM " . $table
                . " WHERE ID=?");
        $stmt->bind_param("s", $id);
        if ($stmt->execute()) {
            $stmt->store_result();
            if ($stmt->num_rows == 1) {
                return true;
            }
        }
        return false;
    }

}
