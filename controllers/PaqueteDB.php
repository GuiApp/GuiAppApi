<?php
/**
 * Description of PaquetePremiumDB
 *
 * @author meza
 */
class PaqueteDB extends EntityDB{
    protected $mysqli;
    const TABLE = 'paquetes';
    
    public function getList(){
        $query = "SELECT * FROM paquetes;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
	
    public function getPaqueteById($idpaquete){
        $query = "SELECT p.*, "
            . "(CASE 1 WHEN c.fecinicio <= NOW() AND c.fecfin >= NOW() THEN 1 ELSE 0 END) AS comprado, "
            . "IFNULL((CONCAT('Activo desde ', DATE_FORMAT(c.fecinicio, '%d/%m/%Y'), ' al ', DATE_FORMAT(c.fecfin, '%d/%m/%Y'))), '')As plazo "
            . "FROM paquetes p "
            . "LEFT JOIN paquetesxcuenta c ON c.idpaquete = p.id "
            . "WHERE p.id=" . $idpaquete;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
	
    public function getFuncionesByIdPaquetes($idfuncion){
        $query = "SELECT * FROM funcionesxpaquete "
                . "WHERE idfuncion = " . $idfuncion;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }

    public function getPaquetesByIdCuenta($idcuenta){
//        $query = "SELECT p.*, 
//                        (CASE 1 WHEN c.fecinicio <= NOW() AND c.fecfin >= NOW()THEN 1 ELSE 0 END) AS comprado, 
//                     IFNULL((CONCAT('Activo desde ', DATE_FORMAT(c.fecinicio, '%d/%m/%Y'), ' al ', DATE_FORMAT(c.fecfin, '%d/%m/%Y'))), '') As plazo FROM paquetes p 
//                LEFT JOIN paquetesxcuenta c ON c.idpaquete = p.id 
//                WHERE (p.activo = 1 OR c.idcuenta = '$idcuenta') AND p.registrado = 0";
//    var_dump($query);
        $query = "SELECT p.id, p.imagen, p.nombre, p.descripcion, p.costo, p.duracion, p.logo, p.appleid, p.activo, p.registrado,
                (CASE WHEN c.fecinicio <= NOW() AND c.fecfin >= NOW() 
                    THEN 1 
                    ELSE 0 
                END) AS comprado,
                IFNULL((CONCAT('Activo desde ', DATE_FORMAT(c.fecinicio, '%d/%m/%Y'), ' al ', DATE_FORMAT(c.fecfin, '%d/%m/%Y'))), '') As plazo 
            FROM paquetes p
            LEFT JOIN paquetesxcuenta c ON c.idpaquete = p.id AND c.idcuenta = 'bWF4aW1vYWxlZGVsb3NyZXllc0BnbWFpbC5jb20'
            WHERE (p.activo = 1) AND p.registrado = 0
            GROUP BY p.id";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
	
    public function insert($imagen=-1, $nombre='',  $descripcion='', 
            $costo=-1, $duracion='', $logo='', 
            $activo=1){
        $query = "INSERT INTO " . self::TABLE . 
                " (imagen, nombre, descripcion, "
                . "costo, duracion, logo, "
                . "activo) "
                . "VALUES "
                . "('$imagen', '$nombre', '$descripcion', "
                . "$costo, $duracion, '$logo', "
                . "$activo);";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, 
            $imagen=-1, $nombre='', $descripcion='', 
            $costo=-1, $duracion='', $logo='', 
            $activo=1) {
        $query = "UPDATE " . self::TABLE . 
            " SET imagen='$imagen', nombre='$nombre', descripcion='$descripcion', "
            . "costo=$costo, duracion=$duracion, logo='$logo', "
            . "activo=$activo "
            . "WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}