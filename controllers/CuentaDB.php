<?php
/**
 * Description of CuentaDB
 *
 * @author meza
 */
class CuentaDB extends EntityDB{
   protected $mysqli;
   const TABLE = 'cuentas';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT c.id, c.nombre, c.email, c.provider, c.notificaciones "
                . "FROM `cuentas` c ");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($id='', $nombre='', $email='', $provider='', $notificaciones=-1){
        $query = "INSERT INTO " . self::TABLE . 
		" (id, nombre, email, provider, notificaciones) "
                . "VALUES "
                . "('$id', '$nombre', '$email', '$provider', $notificaciones);";
        
//        var_dump($query);
        if($this->checkStringID(self::TABLE, $id)){
            return false;
        }
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
         
        $defaulPaq = $this->getDaultPaquete();
        $defaulPaq = $defaulPaq[0]["id"];
//        var_dump($defaulPaq);
        $this->insertPaqueteDefault($id, $defaulPaq);
        return $r;
    }
    
    private function insertPaqueteDefault($id='', $defaulPaq=0) {
        $query = "INSERT INTO paquetesxcuenta 
                (idcuenta, idpaquete, fecinicio, fecfin, abonado, cantimagenes) 
            SELECT '$id',$defaulPaq,'9999-12-31', '1000-01-01', 0, 0 FROM DUAL
            WHERE NOT EXISTS 
              (SELECT id FROM paquetesxcuenta 
              WHERE idcuenta='$id' AND idpaquete=$defaulPaq);";
//        $query = "INSERT INTO paquetesxcuenta 
//                (idcuenta, idpaquete, fecinicio, fecfin, abonado, cantimagenes) 
//            VALUES 
//                ('$id', $defaulPaq, '9999-12-31', '1000-01-01', 0, 0);";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
    
    private function getDaultPaquete() {
        $query = "SELECT id FROM paquetes WHERE registrado = 1";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id='', $nombre='', $email='', $provider='', $notificaciones=-1) {
        if($this->checkStringID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET nombre= '$nombre', email= '$email', "
                    . "provider= '$provider', notificaciones= $notificaciones "
                    . "WHERE id = '$id';");
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}