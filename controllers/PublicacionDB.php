<?php
/**
 * Description of PublicacionDB
 *
 * @author meza
 */
class PublicacionDB extends EntityDB{
   protected $mysqli;
   const TABLE = 'publicaciones';
    
    public function getById($id=0){
        $query="SELECT p.id, p.titulo, p.imagen, p.descripcion, p.idempresa, 
                e.razonsocial, p.destacada, p.fecmodificacion, p.dirty, e.idcuenta 
            FROM publicaciones p 
            LEFT JOIN empresas e ON p.idempresa = e.id 
            WHERE p.id = '$id'";
        
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getListDests(){
        $query = "SELECT p.id, p.titulo, p.imagen, p.descripcion, p.idempresa, "
                . "p.dirty, p.destacada, p.fecmodificacion "
                . "FROM publicaciones p "
                . "WHERE p.destacada = 1 AND p.dirty = 1 "
                . "ORDER BY p.fecmodificacion";
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    /*
     * Usos:
     * - Móvil
     */
    public function getListPubs(){
        $query = "SELECT p.id, p.titulo, p.imagen, p.descripcion, p.idempresa, e.razonsocial, "
                . " (CASE p.dirty WHEN 1 THEN 'SI' ELSE 'NO' END) AS dirty, p.destacada, p.fecmodificacion, p.dirty "
                . "FROM publicaciones p "
                . "LEFT JOIN empresas e ON p.idempresa = e.id "
                . "WHERE p.destacada = 0 AND p.dirty = 1 "
                . "ORDER BY p.fecmodificacion";
        
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    /*
     * Usos:
     * - Móvil
     */
    public function listByIdCuenta($idcuenta){
        $query = "SELECT p.id, p.titulo, p.imagen, p.descripcion, p.idempresa, e.razonsocial, "
                . " (CASE p.dirty WHEN 1 THEN 'SI' ELSE 'NO' END) AS dirty, p.destacada, p.fecmodificacion, p.dirty, e.idcuenta "
                . "FROM publicaciones p "
                . "LEFT JOIN empresas e ON p.idempresa = e.id "
                . "WHERE p.destacada = 0 AND e.idcuenta = '$idcuenta' "
                . "ORDER BY p.fecmodificacion";
        
        //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    /*
     * Usos:
     * - Móvil
     */
    public function listByIdEmpresa($idempresa){
        $query = "SELECT p.id, p.titulo, p.imagen, p.descripcion, p.idempresa, e.razonsocial, "
                . " (CASE p.dirty WHEN 1 THEN 'SI' ELSE 'NO' END) AS dirty, p.destacada, p.fecmodificacion, p.dirty, e.idcuenta "
                . "FROM publicaciones p "
                . "LEFT JOIN empresas e ON p.idempresa = e.id "
                . "WHERE e.id = '$idempresa' "
                . "ORDER BY p.fecmodificacion";
        
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    /*
     * Usos:
     * - BackOffice
     */
    public function getList(){
        $result = $this->mysqli->query(
                 "SELECT p.id, p.titulo, p.imagen, p.descripcion, p.idempresa, "
                . "IFNULL(e.razonsocial, '') AS razonsocial, (CASE p.dirty WHEN 1 THEN 'SI' ELSE 'NO' END) AS dirty, "
                . "p.destacada, p.fecmodificacion, p.dirty "
                . "FROM publicaciones p "
                . "LEFT JOIN empresas e ON p.idempresa = e.id "
                . "ORDER BY p.fecmodificacion");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $id='', $titulo='', $imagen='', 
            $descripcion='', $idempresa=-1, $dirty=-1, 
            $destacada=-1){
        
        $query = "INSERT INTO " . self::TABLE . 
                " (id, titulo, imagen, "
                . "descripcion, idempresa, dirty, "
                . "destacada, fecmodificacion) "
                . "VALUES ("
                . "'$id', '$titulo', '$imagen', "
                . "'$descripcion', '$idempresa', $dirty, "
                . "$destacada, NOW());";
        //var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
        
    /*
     * Usos:
     * - Móvil
     */
    public function insertMovil(
            $id='', $titulo='', $imagen='', 
            $descripcion='', $idempresa=-1, $dirty=0, 
            $destacada=-1){
        
        $file = $_SERVER['DOCUMENT_ROOT'] . "/" . $this->conf['imagestock'] . "/pub_$id.png";
        file_put_contents($file, base64_decode($imagen));
        $file = $this->conf['protocol'] . $_SERVER['SERVER_NAME'] . "/" . $this->conf['imagestock'] . "/pub_$id.png";
        
        $query = "INSERT INTO publicaciones
                (id, titulo, imagen, 
                descripcion, idempresa, dirty, 
                destacada, fecmodificacion) 
            VALUES (
                '$id', '$titulo', '$file', 
                '$descripcion', '$idempresa', $dirty, 
                $destacada, NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }

    public function insertImage($id, $imagen) {
        $ini = parse_ini_file('conf.ini');
        
//        $file = $_SERVER['DOCUMENT_ROOT'] . "/" . $ini['imagestock'] . "/pub_$id.png";
//        file_put_contents($file, base64_decode($imagen));
//        
//        $file = $ini['protocol'] . $ini['server'] . "/" . $ini['imagestock'] . "/pub_$id.png";
        $file = $_SERVER['DOCUMENT_ROOT'] . "/" . $this->conf['imagestock'] . "/pub_$id.png";
        file_put_contents($file, base64_decode($imagen));
        
        $file = $this->conf['protocol'] . $_SERVER['SERVER_NAME'] . "/" . $this->conf['imagestock'] . "/pub_$id.png";
        
        $query = "UPDATE " . self::TABLE . " SET imagen='$file' "
                    . "WHERE id = '$id';";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        return $r;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('s', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update(
        $id='', $idempresa='', $titulo='', 
        $imagen='', $descripcion='', $destacada=-1, $dirty=-1) {

        if($this->checkID($id)){
            $query="UPDATE " . self::TABLE . " SET "
                . "idempresa= '$idempresa', titulo= '$titulo', imagen= '$imagen', "
                . "descripcion= '$descripcion', destacada= $destacada, fecmodificacion= NOW(), "
                . "dirty= $dirty "
                . "WHERE id = '$id' ";
          // var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        } else
            return false;
    }

    public function updateImage($id, $imagen) {
        $ini = parse_ini_file('conf.ini');
        
        $file = $_SERVER['DOCUMENT_ROOT'] . "/" . $ini['imagestock'] . "/pub_$id.png";
        file_put_contents($file, base64_decode($imagen));
        
        $file = $ini['protocol'] . $_SERVER['SERVER_NAME'] . "/" . $ini['imagestock'] . "/pub_$id.png";
        $query = "UPDATE " . self::TABLE . " SET imagen='$file' "
                    . "WHERE id = '$id';";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        return $r;
    }
        
    /*
     * Usos:
     * - Móvil
     */    
    public function updateMovil(
        $id='', $idempresa='', $titulo='', 
        $imagen='', $descripcion='', $destacada=-1, $dirty=-1) {

        if($this->checkID($id)){
            $file = $_SERVER['DOCUMENT_ROOT'] . "/" . $this->conf['imagestock'] . "/pub_$id.png";
            file_put_contents($file, base64_decode($imagen));
            $file = $this->conf['protocol'] . $_SERVER['SERVER_NAME'] . "/" . $this->conf['imagestock'] . "/pub_$id.png";
            
            $query="UPDATE publicaciones SET 
                idempresa= '$idempresa', titulo= '$titulo', imagen= '$file', 
                descripcion= '$descripcion', destacada= $destacada, fecmodificacion= NOW(), 
                dirty= $dirty 
                WHERE id = '$id';";
//           var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        } else
            return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("s", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
    
    public function putImage ($id = -1, $image = '') {  {
            $stmt = $this->mysqli->prepare("UPDATE image "
                    . "WHERE id = ?;");          
            $stmt->bind_param('si', $image, $id);
            $r = $stmt->execute();
            $stmt->close();
            return $r;
        }
        return false;
    } 
    
    public function postImage ($id = -1, $image = '') {
        $stmt = $this->mysqli->prepare(
                "INSERT INTO"  . self::TABLE .  
                "(id, image) "
                . "VALUES ($id, '$image');");
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
}