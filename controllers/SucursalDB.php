<?php
/**
 * Description of SucursalDB
 *
 * @author meza
 */
class SucursalDB extends EntityDB{
    protected $mysqli;
    const TABLE = 'sucursales';
       
    public function getById($id=0){
        $query = "SELECT s.id, s.idempresa, e.razonsocial, s.direccion, 
                s.idpais, s.idprovincia, s.idlocalidad, s.telefono, 
                s.delivery, s.veinticuatrohs, s.diashorarios, s.dirty, 
                s.palabrasclave, s.latitud, s.longitud 
            FROM sucursales s 
            LEFT JOIN empresas e ON e.id = s.idempresa
            WHERE s.id = '$id'";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT s.id, s.idempresa, e.razonsocial, s.direccion, s.idpais, s.idprovincia,"
              . " s.idlocalidad, s.telefono, s.delivery, s.veinticuatrohs, s.diashorarios,"
              . " s.dirty, s.palabrasclave, s.latitud, s.longitud "
                . "FROM sucursales s "
                . "LEFT JOIN empresas e ON e.id = s.idempresa");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    
    public function getListXEmpresa($idempresa){
        $result = $this->mysqli->query(
                "SELECT s.id, s.idempresa, e.razonsocial, e.estado, s.direccion, "
                . "s.idpais, s.idprovincia, s.idlocalidad, s.telefono, s.delivery, "
                . "s.veinticuatrohs, s.diashorarios, s.dirty, s.palabrasclave, s.latitud, "
                . "s.longitud "
                . "FROM sucursales s "
                . "LEFT JOIN empresas e ON s.idempresa = e.id "
                . "WHERE s.idempresa = '" . $idempresa . "'");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    /*$obj->idempresa, $obj->direccion, $obj->telefono, 
                    $obj->latitud, $obj->longitud,  $obj->delivery, 
                    $obj->veinticuatrohs, $obj->diashorarios, $obj->dirty*/
    //No acepta el campo "24hs" por tener numeros
    public function insert($id, $idempresa='', $direccion='', 
            $telefono='', $latitud=0, $longitud=0, 
            $delivery=0, $veinticuatrohs=0, $diashorarios="", 
            $dirty=1){
        $query ="INSERT INTO " . self::TABLE . " "
                . "(id, idempresa, direccion, "
                . "telefono, latitud, longitud, "
                . "delivery, veinticuatrohs, diashorarios, "
                . "dirty) "
                . "VALUES ('$id', '$idempresa', '$direccion', "
                . "'$telefono', $latitud, $longitud, "
                . "$delivery, $veinticuatrohs, '$diashorarios', "
                . "$dirty);"; 
        //var_dump($query);
        
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    /*$_GET['id'], 
                    $obj->idempresa, $obj->direccion,
                    $obj->latitud, $obj->longitud, 
                    $obj->diashorarios, $obj->veinticuatrohs,
                    $obj->delivery, $obj->telefono, 
                    $obj->dirty*/
    public function update($id=-1, 
            $idempresa='', $direccion='', 
            $latitud=-1, $longitud=-1, 
            $diashorarios=-1, $veinticuatrohs=-1, 
            $delivery=-1, $telefono='', 
            $dirty='') {
        
        if($this->checkID($id)){
            $query = "UPDATE " . self::TABLE . " SET idempresa='$idempresa', direccion='$direccion', "
                    . "latitud=$latitud, longitud=$longitud, "
                    . "diashorarios='$diashorarios', veinticuatrohs=$veinticuatrohs, "
                    . "delivery=$delivery, telefono='$telefono', "
                    . "dirty=$dirty "
                    . "WHERE id = '$id';";
//            var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        else
            return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID='$id'");
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}