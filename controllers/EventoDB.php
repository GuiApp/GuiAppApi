<?php
/**
 * Description of EventoDB
 *
 * @author meza
 */
class EventoDB extends EntityDB{
    protected $mysqli;
    const TABLE = 'eventos';

    public function getById($id = "") {
        $query = "SELECT e.id, e.nombre, e.descripcion, e.imagen, 
            e.idtipo, t.tipo, e.ubicacion, e.fecinicio, e.fecfin, 
            e.idempresa, em.razonsocial, e.horainicio, e.horafin, 
            em.idcuenta, e.dirty 
            FROM eventos e 
            LEFT JOIN eventostipos t ON e.idtipo = t.id 
            LEFT JOIN empresas em ON e.idempresa = em.id 
            WHERE e.id = '$id'
            ORDER BY e.fecinicio";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }

    public function getList() {
        $query = "SELECT e.id, e.nombre, e.descripcion, e.imagen, e.idtipo, 
                t.tipo, e.ubicacion, e.fecinicio, e.fecfin, e.idempresa, 
                em.razonsocial, e.horainicio, e.horafin, em.idcuenta, e.dirty 
            FROM eventos e 
            LEFT JOIN eventostipos t On e.idtipo = t.id 
            LEFT JOIN empresas em ON e.idempresa = em.id 
            WHERE e.dirty = 1 AND NOW() < CONCAT(e.fecinicio, ' ', e.horainicio)
            ORDER BY e.fecinicio;";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }

    public function getListBackoffice() {
        $query = "SELECT e.id, e.nombre, e.descripcion, e.imagen, e.idtipo, 
                t.tipo, e.ubicacion, e.fecinicio, e.fecfin, e.idempresa, 
                em.razonsocial, e.horainicio, e.horafin,em.idcuenta, e.dirty 
            FROM eventos e 
            LEFT JOIN eventostipos t On e.idtipo = t.id 
            LEFT JOIN empresas em ON e.idempresa = em.id 
            ORDER BY e.dirty, e.fecinicio;";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }

    public function getByIdEmpresa($idempresa) {
        $query = "SELECT e.id, e.nombre, e.descripcion, e.imagen, "
                . "e.idtipo, t.tipo, e.ubicacion, e.fecinicio, e.fecfin, "
                . "e.idempresa, em.razonsocial, e.horainicio, "
                . "e.horafin, em.idcuenta, e.dirty "
                . "FROM eventos e "
                . "LEFT JOIN eventostipos t On e.idtipo = t.id "
                . "LEFT JOIN empresas em ON e.idempresa = em.id "
                . "WHERE e.idempresa = '$idempresa' "
                . "ORDER BY e.fecinicio";
        //var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }

    public function getFiltered($idtipo=-1, $fecha="-1") {
        $query = "SELECT e.id, e.nombre, e.descripcion, e.imagen, 
                e.idtipo, t.tipo, e.ubicacion, e.fecinicio, e.fecfin, 
                e.idempresa, em.razonsocial, e.horainicio, 
                e.horafin, e.dirty 
            FROM eventos e 
            LEFT JOIN eventostipos t On e.idtipo = t.id 
            LEFT JOIN empresas em ON e.idempresa = em.id 
            WHERE NOW() <= e.fecinicio AND e.dirty = 1 ";
        if ($idtipo > -1) {
            $query .= "AND e.idtipo = $idtipo ";
        } else {
            $query .= "AND 1 ";
        }
        if ($fecha > -1) {
            $query .= "AND e.fecinicio = '$fecha' ";
        } else {
            $query .= "AND 1 ";
        }
        $query .= "ORDER BY e.fecinicio;";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }

    public function insert($id='',
            $nombre = '', $descripcion = '', $imagen = '', 
            $idtipo = -1, $ubicacion = '', $fecinicio = '', $fecfin = '', 
            $idempresa = '', $horainicio = '', $horafin = '', $dirty=0) {
        $query = "INSERT INTO " . self::TABLE . 
                " (id, "
                . "nombre, descripcion, imagen, "
                . "idtipo, ubicacion, fecinicio, fecfin, "
                . "idempresa, horainicio, horafin, dirty) "
                . "VALUES ('$id', "
                . "'$nombre', '$descripcion', '$imagen', "
                . "$idtipo, '$ubicacion', '$fecinicio', "
                . "'$fecfin', '$idempresa', '$horainicio', '$horafin', $dirty);";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();

        $stmt->close();
        return $r;
    }

    public function insertImage($id, $imagen) {
        $ini = parse_ini_file('conf.ini');
        $file = $_SERVER['DOCUMENT_ROOT'] . "/" . $this->conf['imagestock'] . "/eve_$id.png";
        file_put_contents($file, base64_decode($imagen));
        
        $file = $this->conf['protocol'] . $_SERVER['SERVER_NAME'] . "/" . $this->conf['imagestock'] . "/eve_$id.png";
        $query = "UPDATE " . self::TABLE . " SET imagen='$file' WHERE id = '$id';";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        return $r;
    }

    public function update($id = -1, 
            $nombre = '', $descripcion = '', $imagen = '', 
            $idtipo = -1, $ubicacion = '', $fecinicio = '', 
            $fecfin = '', $idempresa = '', $horainicio = '', 
            $horafin = '', $dirty=0) {
        $query = "UPDATE " . self::TABLE . 
                " SET nombre='$nombre', descripcion='$descripcion', imagen='$imagen', "
                . "idtipo=$idtipo, ubicacion='$ubicacion', fecinicio='$fecinicio', "
                . "fecfin='$fecfin', idempresa='$idempresa', horainicio='$horainicio', "
                . "horafin='$horafin', dirty=$dirty "
                . "WHERE id = '$id';";
        if ($this->checkStringID(self::TABLE, $id)) {
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute();
            $stmt->close();
            return $r;
        }
        return false;
    }

    public function updateImage($id, $imagen) {
        $ini = parse_ini_file('conf.ini');
        $file = $_SERVER['DOCUMENT_ROOT'] . "/" . $this->conf['imagestock'] . "/eve_$id.png";
        file_put_contents($file, base64_decode($imagen));
        
        $file = $this->conf['protocol'] . $_SERVER['SERVER_NAME'] . "/" . $this->conf['imagestock'] . "/eve_$id.png";
        $query = "UPDATE " . self::TABLE . " SET imagen='$file' WHERE id = '$id';";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        return $r;
    }

    public function delete($id = 0) {
        $stmt = $this->mysqli->prepare("DELETE FROM " . self::TABLE . " WHERE id = '$id';");
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
}
