<?php
/**
 * Description of VidrieraVirtualDB
 *
 * @author meza
 */
class ImagenDB extends EntityDB{
   protected $mysqli;
   const TABLE = 'imagenes';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id='$id';");
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT id, idempresa, url FROM imagenes");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdEmpresa($idempresa = ''){
        $result = $this->mysqli->query(
                "SELECT id, idempresa, url 
                FROM imagenes 
                WHERE idempresa  = '$idempresa'");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($idempresa='', $url='', $imagen=''){
        $id = $this->gen_uuid();
        
        $file = $_SERVER['DOCUMENT_ROOT'] . "/" . $this->conf['imagestock'] . "/gal_$id.png";
        file_put_contents($file, base64_decode($imagen));
        $url = $this->conf['protocol'] . $_SERVER['SERVER_NAME'] . "/" . $this->conf['imagestock'] . "/gal_$id.png";
        
        $stmt1 = $this->mysqli->prepare(
                "INSERT INTO imagenes (id, idempresa, url) 
                VALUES ('$id', '$idempresa', '$url');");
        $r = $stmt1->execute();
        $stmt1->close();
        return $r;
    }
    
    public function update($id='', $idempresa='', $url='', $imagen='') {
        if($this->checkStringID(self::TABLE, $id)){
            $file = $_SERVER['DOCUMENT_ROOT'] . "/" . $this->conf['imagestock'] . "/gal_$id.png";
            file_put_contents($file, base64_decode($imagen));
            $url = $this->conf['protocol'] . $_SERVER['SERVER_NAME'] . "/" . $this->conf['imagestock'] . "/gal_$id.png";
        
            $query = "UPDATE imagenes
                SET idempresa='$idempresa', url='$url' 
                WHERE id = '$id';";
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute();
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}