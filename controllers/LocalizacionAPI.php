<?php
/**
 * Description of LocalizacionAPI
 *
 * @author meza
 */
class LocalizacionAPI extends EntityAPI {
    const API_ACTION = 'localizacion';
    const PREFIX_PAIS = 'p';
    const PREFIX_PROV = 'r';
    const PREFIX_LOC = 'l';
    const PREFIX_MUNIC = 'm';
	
     public function API(){
        header('Content-Type: application/JSON');    
        $method = $_SERVER['REQUEST_METHOD'];
		$this->db = new LocalizacionDB();
		
        switch ($method) {
            case 'GET'://consulta
                $this->processGet();
                break;
}
}    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        if($id)
        {
            $isPais = strpos($id, self::PREFIX_PAIS);
            $isProv = strpos($id, self::PREFIX_PROV);
            $isLoc = strpos($id, self::PREFIX_LOC);
            $isMunic = strpos($id, self::PREFIX_MUNIC);

            if ($isPais !== false) {
                $response = $this->db->getPaisList();
                echo json_encode($response,JSON_PRETTY_PRINT);
            } elseif ($isProv !== false) {
                $response = $this->db->getProvListByPais(substr( $id, 1));
                echo json_encode($response,JSON_PRETTY_PRINT);
            } elseif ($isLoc !== false) {
                $response = $this->db->getLocListByProv(substr( $id, 1));
                echo json_encode($response,JSON_PRETTY_PRINT);
            } elseif ($isMunic !== false) {
                $response = $this->db->getMunicListByLoc(substr( $id, 1));
                echo json_encode($response,JSON_PRETTY_PRINT);
            }else{
                $response = $this->db->getById($id);
                echo json_encode($response,JSON_PRETTY_PRINT);                    
            }
        }
        else
            $this->response(400);
    }
}
