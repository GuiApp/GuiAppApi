<?php
/**
 * Utilizado SOLO en iOS
 */
class PagoAPI extends EntityAPI {
    const API_ACTION = 'pago'; 
    
    public function __construct() {
	$this->db = new PagoDB();
        $this->fields = [];
        array_push($this->fields, 
                'idcuenta', 
                'appleid');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        if(!$id){
            $this->response(400);
            exit;
        }
        $isList = ($id == self::GET_LISTA);
        $isPaquete = ($id == self::GET_PAQUETE);
        $isFuntion = ($id == self::GET_FUNCIONES);
        $isCuenta = ($id == self::GET_CUENTA);
        if ($isList) {
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isPaquete) {
            $idpaquete = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getPaqueteById($idpaquete);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isFuntion) {
            $response = $this->db->getFuncionesByIdPaquetes(substr( $id, 1));
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isCuenta) {
            $idcuenta = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getPaquetesByIdCuenta($idcuenta);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);                    
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert($obj->idcuenta, $obj->appleid);
        if($r) {$this->response(200,"success","new record added"); }
        else {$this->response(204,"error","No record added"); }
    }
	
    function processPut() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idcuenta, $obj->idcuenta);
        
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}