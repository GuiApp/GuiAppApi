<?php
/**
 * Description of EventoAPI
 *
 * @author meza
 */
class EventoAPI  extends EntityAPI {
    const API_ACTION = 'evento';
    const GET_LISTA = 'lista';
    const GET_LISTABACKOFFICE = 'listabackoffice';
    const GET_BYIDEMPRESA = 'byidempresa';
    const GET_FILTERED = 'filtered';
    const POST_IMAGEN = 'imagen';
    const POST_FROMMOBILE = 'postfrommobile';
    const PUT_IMAGEN = 'imagen';
    const PUT_FROMMOBILE = 'putfrommobile';
    
    public function __construct() {
	$this->db = new EventoDB();
        $this->fields = [];
        array_push($this->fields, 
            'id',
            'nombre',
            'descripcion',
            'imagen',
            'idtipo',
            'ubicacion',
            'fecinicio',
            'fecfin',
            'idempresa',
            'horainicio',
            'horafin');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isLista = isset($id) ? ($id === self::GET_LISTA) : false;
        $isListaBackoofice = isset($id) ? ($id === self::GET_LISTABACKOFFICE) : false;
        $isByIdEmpresa = isset($id) ? ($id === self::GET_BYIDEMPRESA) : false;
        $isFiltered = isset($id) ? ($id === self::GET_FILTERED) : false;
        
        if($isLista) {
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($isListaBackoofice) {
            $response = $this->db->getListBackoffice();
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($isByIdEmpresa){
            $idempresa = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdEmpresa($idempresa);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($isFiltered){
            $idtipo = filter_input(INPUT_GET, 'fld1');
            $fecha = filter_input(INPUT_GET, 'fld2');
            $response = $this->db->getFiltered($idtipo, $fecha);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $id = filter_input(INPUT_GET, 'id');
        $isImagen = isset($id) ? ($id === self::POST_IMAGEN) : false;
        
        if($isImagen) {
//            echo 'capo';
            $this->postImage();
        } else {
            $this->postData();
        }
    }
    
    function postData() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $dirty = isset($obj->dirty) ? ($obj->dirty) : (0);
        $r = $this->db->insert($obj->id, 
                $obj->nombre, $obj->descripcion, $obj->imagen, 
                $obj->idtipo, $obj->ubicacion, $obj->fecinicio, 
                $obj->fecfin, $obj->idempresa, $obj->horainicio, 
                $obj->horafin, $dirty);
        
        if($r) {$this->response(200,"success","new record added"); }
        else { $this->response(204,"success","record duplicated"); }
    }
    
    function postImage() {
        $obj = json_decode(file_get_contents('php://input'));
//        var_dump($obj);
        $r = $this->db->insertImage($obj->id, $obj->image);
        if ($r) {
            $this->response(200, "success", "new record added");
        }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        $isImagen = isset($id) ? ($id === self::PUT_IMAGEN) : false;
        
        if($isImagen) {
            $this->putImage();
        } else {
            $this->putData();
        }
    }
    
    function putData() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $dirty = isset($obj->dirty) ? ($obj->dirty) : (0);
        $r = $this->db->update($id,
                $obj->nombre, $obj->descripcion, $obj->imagen, 
                $obj->idtipo, $obj->ubicacion, $obj->fecinicio, 
                $obj->fecfin, $obj->idempresa, $obj->horainicio, 
                $obj->horafin, $dirty);
        
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
    
    function putImage() {
        $obj = json_decode(file_get_contents('php://input'));
        $r = $this->db->updateImage($obj->id, $obj->image);
        if ($r) {
            $this->response(200, "success", "new record added");
        }
    }
}