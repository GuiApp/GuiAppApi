<?php
/**
 * Description of SponsorAPI
 *
 * @author meza
 */
class SponsorAPI extends EntityAPI {
    const API_ACTION = 'sponsor';

     public function __construct() {
	$this->db = new SponsorDB();
    }
    
    function processGet(){
        if($_GET['action']==self::API_ACTION){
            if(isset($_GET['id'])){
                $response = $this->db->getById($_GET['id']);
                echo json_encode($response,JSON_PRETTY_PRINT);
            }else{
                $response = $this->db->getList();
                echo json_encode($response,JSON_PRETTY_PRINT);
            }
        }else{
                $this->response(400);
        }
    } 
	
    function processPost() {
        if($_GET['action']==self::API_ACTION) {
            $obj = json_decode( file_get_contents('php://input') );
            $objArr = (array)$obj;
            if (empty($objArr)) {
                $this->response(422,"error","Nothing to add. Check json");
            } else if(isset($obj->razonsocial)) {
                $r = $this->db->insert($obj->razonsocial);
                if($r)
                    $this->response(200,"success","new record added");
            } else {
                $this->response(422,"error","The property is not defined");
            } 
        } else {
            $this->response(400);
        }
    }
    
    function processPut() {
        if( isset($_GET['action']) && isset($_GET['id']) ){
            if($_GET['action']==self::API_ACTION){
                $obj = json_decode( file_get_contents('php://input') );   
                $objArr = (array)$obj;
                if (empty($objArr)){                        
                    $this->response(422,"error","Nothing to add. Check json");                        
                }else if(isset($obj->razonsocial)) {
                    if($this->db->update($_GET['id'], $obj->razonsocial))
                        $this->response(200,"success","Record updated");                             
                    else
                        $this->response(304,"success","Record not updated");                             
                }else{
                    $this->response(422,"error","The property is not defined");                        
                }     
                exit;
           }
        }
        $this->response(400);
    }
}
