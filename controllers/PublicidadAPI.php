<?php
/**
 * Description of PublicidadAPI
 *
 * @author meza
 */
class PublicidadAPI extends EntityAPI {
    const API_ACTION = 'publicidad';
    const PREFIX_RANDOM = 'r';

     public function __construct() {
	$this->db = new PublicidadDB();
    }
	
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
                
        if($id) {
            $isRandom = strpos($id, self::PREFIX_RANDOM);

            if ($isRandom !== false) {
                $response = $this->db->getRandom();//Order by Cronologicamente
                echo json_encode($response,JSON_PRETTY_PRINT);
            } else {
                $response = $this->db->getById($id);
                echo json_encode($response,JSON_PRETTY_PRINT);                    
            }
        } else {
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
     }
	
    function processPost() {
        if($_GET['action']==self::API_ACTION) {
            $obj = json_decode( file_get_contents('php://input') );
            $objArr = (array)$obj;
            if (empty($objArr)) {
                $this->response(422,"error","Nothing to add. Check json");
            } else if (isset($obj->idsponsor)
                        AND ($obj->fecinicio) AND ($obj->fecfin)) {
                $r = $this->db->insert($obj->idsponsor,
                        $obj->fecinicio, $obj->fecfin, $obj->imagen, $obj->url);
                if($r)
                    $this->response(200,"success","new record added");
            } else {
                $this->response(422,"error","The property is not defined");
            } 
        } else {
            $this->response(400);
        }
    }
		
    function processPut() {
        if( isset($_GET['action']) && isset($_GET['id']) ){
            if($_GET['action']==self::API_ACTION){
                $obj = json_decode( file_get_contents('php://input') );   
                $objArr = (array)$obj;
                if (empty($objArr)) {                        
                    $this->response(422,"error","Nothing to add. Check json");                        
                } elseif (isset($obj->idsponsor) AND
                        isset($obj->fecinicio) AND isset($obj->fecfin)) {
                    if($this->db->update($_GET['id'], $obj->idsponsor,
                            $obj->fecinicio, $obj->fecfin, $obj->imagen, $obj->url))
                        $this->response(200,"success","Record updated");                             
                    else
                        $this->response(304,"success","Record not updated");                             
                }else{
                    $this->response(422,"error","The property is not defined");                        
                }     
                exit;
           }
        }
        $this->response(400);
    }
}