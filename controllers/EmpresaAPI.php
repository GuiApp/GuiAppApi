<?php
/**
 * Description of ContatoAPI
 *
 * @author meza
 */
class EmpresaAPI extends EntityAPI {
    const API_ACTION = 'empresa';
    const GET_DETALLE = 'detalle'; //CAMBIE el 09/04/2018
    const GET_VIDRIERA = 'v';
    const GET_XCUENTA = 'xcuenta';
    const GET_LISTADO = 'l';
    const GET_BYID = 'byid';
    const GET_CATEGORIAS = 'categorias';
    const PATCH_BUSQUEDA = 'b';
    const PATCH_XCATEGORIA = 'c';
    const PATCH_XCUENTA = 'xcuenta';
    const POST_FROMMOBILE = 'frommobile';
    const POST_CATEGORIAS = 'c';
    const PUT_CATEGORIAS = 'c';
    const POST_IMAGEN = 'imagen';
    const PUT_IMAGEN = 'imagen'; //AGREGUE el 10/09/04/2018

     public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
		$this->db = new EmpresaDB();
		
        switch ($method) {
            case 'GET'://consulta
                $this->processGet();
                break;
            case 'POST'://inserta
                $this->processPost();
                break;
            case 'PUT'://actualiza
                $this->processPut();
                break;
            case 'DELETE'://elimina
                $this->proceesDelete();
                break;
            case 'PATCH'://metodos de procesos
                $this->processPatch();
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }

    function processGet() {
        $id = filter_input(INPUT_GET, 'id');
        if ($id) {
            $isDetalle = strpos($id, self::GET_DETALLE);
            $isVidriera = strpos($id, self::GET_VIDRIERA);
            $isXCuenta = strpos($id, self::GET_XCUENTA);
            $isLista = strpos($id, self::GET_LISTADO);
            $isById = strpos($id, self::GET_BYID);
            $isCategorias = strpos($id, self::GET_CATEGORIAS);
            if ($isDetalle !== false) {
                $response = $this->db->getDetalleByIdSuc(substr($id, 1));
                echo json_encode($response, JSON_PRETTY_PRINT);
            } elseif ($isVidriera !== false) {
                $response = $this->db->getVidrieraByIdSuc(substr($id, 1));
                echo json_encode($response, JSON_PRETTY_PRINT);
            } elseif ($isXCuenta !== false) {
                $idcuenta = filter_input(INPUT_GET, 'fld1');
                $response = $this->db->getEmpresasByCuentas($idcuenta);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } elseif ($isLista !== false) {
                $response = $this->db->getList();
                echo json_encode($response, JSON_PRETTY_PRINT);
            } elseif($isById !== false) {                
                $response = $this->db->getById($_GET['fld1']);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } elseif($isCategorias !== false) {                
                $response = $this->db->getCategorias($_GET['fld1']);
                echo json_encode($response, JSON_PRETTY_PRINT);
            }
        }
    }

    /*
     * Estas funciones son utilizadas desde la app móvil.
     * Necesita que se encierre entre corchetes ("[]") los parámetros de entrada
     */
    function processPatch() {
        $id = filter_input(INPUT_GET, 'id');
        $obj = json_decode(file_get_contents('php://input'));
        $objArr = (array) $obj;
        //var_dump($objArr[0]->latitud);

        if (!$id) {
            return $this->response(400);
        }
            
        $isBusqueda = substr($id, 0, strlen(self::PATCH_BUSQUEDA)) === self::PATCH_BUSQUEDA;
        $isXCategoria = substr($id, 0, strlen(self::PATCH_XCATEGORIA)) === self::PATCH_XCATEGORIA;
        $isXCuenta = substr($id, 0, strlen(self::PATCH_XCUENTA)) === self::PATCH_XCUENTA;
        
        if ($isBusqueda !== false) {   
            if (isset($objArr[0]->param) &&  isset($objArr[0]->latitud) && isset($objArr[0]->longitud)) {
                $response = $this->db->busqueda($objArr[0]->param, $objArr[0]->latitud, $objArr[0]->longitud);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } else {
                $this->response(400);
            }
        } elseif ($isXCategoria !== false) {
            if (isset($obj[0]->idcategoria) && isset($obj[0]->param) && 
                    isset($obj[0]->latitud) && isset($obj[0]->longitud)) {
                $response = $this->db->xcategoria(
                        $obj[0]->idcategoria, $obj[0]->param, 
                        $obj[0]->latitud, $obj[0]->longitud);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } else {
                $this->response(400);
            }
        } elseif ($isXCuenta !== false) {
            //var_dump($obj[0]->idcuenta);
            if (isset($obj[0]->idcuenta)) {
                $response = $this->db->getEmpresasByCuentas($obj[0]->idcuenta);
                echo json_encode($response, JSON_PRETTY_PRINT);
            } else {
                $this->response(400);
            }
        } else {
            $this->response(400);
        }
    }

    function processPost() {
        $id = filter_input(INPUT_GET, 'id');
        if ($id) {
            $isFromMobile = isset($id) ? $id === self::POST_FROMMOBILE : false;
            $isXCategoria = strpos($id, self::POST_CATEGORIAS);
            $isImagen = strpos($id, self::POST_IMAGEN);
            if ($isXCategoria !== false) { $this->postCategoriasXEmpresa(); } 
            elseif ($isImagen !== false) { $this->postImage(); }
            elseif ($isFromMobile !== false) { $this->postFromMobile(); }
        } else { $this->postEmpresa(); }
    }
    
    function postImage() {
        $obj = json_decode(file_get_contents('php://input'));
        $r = $this->db->insertImage($obj->id, $obj->image);
        if ($r) { $this->response(200, "success", "new record added"); }
    }

    function postEmpresa() {
        $obj = json_decode(file_get_contents('php://input'));
        $objArr = (array) $obj;
        
        if (empty($objArr)) { $this->response(422, "error", "Nothing to add. Check json"); } 
        elseif (isset($obj->id) AND isset($obj->razonsocial)) {
            $dirty = 0;
            $r = $this->db->insert($obj->id, $obj->razonsocial, $obj->cuit, $obj->mediodepago, 
                    $obj->estado, $obj->logo, $obj->facebook,  $obj->twitter, 
                    $obj->instagram, $obj->palabrasclave,  $obj->web, $dirty, 
                    $obj->idcuenta);
        
            if ($r) { $this->response(200, "success", "new record added"); } 
            else { $this->response(422,"error","Duplicado"); }
        } else { $this->response(422, "error", "The property is not defined"); }
    }

    function postFromMobile() {
        $obj = json_decode(file_get_contents('php://input'));
        $objArr = (array) $obj;
        
        if (empty($objArr)) { $this->response(422, "error", "Nothing to add. Check json"); } 
        elseif (isset($obj->id) AND isset($obj->razonsocial) AND isset($obj->logo)) {
            $dirty = 0;
            $r = $this->db->insertFromMovil($obj->id, $obj->razonsocial, $obj->cuit, 
                    $obj->mediodepago, $obj->estado, $obj->logo, 
                    $obj->facebook,  $obj->twitter, $obj->instagram, 
                    $obj->palabrasclave,  $obj->web, $dirty, 
                    $obj->idcuenta, $obj->categorias);
        
            if ($r) { $this->response(200, "success", "new record added"); } 
            else { $this->response(422,"error","Duplicado"); }
        } else { $this->response(422, "error", "The property is not defined"); }
    }

    function postCategoriasXEmpresa() {
        $obj = json_decode(file_get_contents('php://input'));
        $objArr = (array) $obj;
        
        if (empty($objArr)) { $this->response(422, "error", "Nothing to add. Check json"); } 
        elseif (isset($obj->idempresa) AND isset($obj->idcategoria)) {
            $r = $this->db->insertCXE($obj->idempresa, $obj->idcategoria);
            if ($r) { $this->response(200, "success", "new record added"); }
        } else { $this->response(422, "error", "The property is not defined"); }
    }

    function processPut() {
        $obj = json_decode( file_get_contents('php://input') );   
        $objArr = (array)$obj;
        $id = filter_input(INPUT_GET, 'id');

        if (empty($objArr)){                        
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        
        if ($id) {
            $isImagen = strpos($id, self::PUT_IMAGEN);
            $isXCategoria = substr($id, 0, strlen(self::POST_CATEGORIAS)) === self::POST_CATEGORIAS;
            $isFromMobile = isset($id) ? $id === self::POST_FROMMOBILE : false;
        
            if ($isXCategoria !== false) { $this->putCategoriaXEmpresa(); }
            elseif($isImagen !== false) { $this->putImage();}
            elseif ($isFromMobile !== false) { $this->putFromMobile(); }
            else { $this->putEmpresa(); }
        } else $this->response(400);
    }
    
    function putEmpresa() {
        $obj = json_decode(file_get_contents('php://input'));
        $objArr = (array) $obj;
        if (empty($objArr)) { $this->response(422, "error", "Nothing to add. Check json"); } 
        else if (isset($obj->razonsocial)) {
            if ($this->db->update($_GET['id'], $obj->razonsocial, $obj->cuit, 
                    $obj->mediodepago, $obj->estado, $obj->logo, 
                    $obj->facebook, $obj->twitter, $obj->instagram, 
                    $obj->palabrasclave, $obj->web, 
                    $obj->dirty, $obj->idcuenta))
                $this->response(200, "success", "Record updated");
            else { $this->response(304, "success", "Record not updated");}
        } else { $this->response(422, "error", "The property is not defined"); }
        exit;
    }
    
    function putFromMobile() {
        $obj = json_decode(file_get_contents('php://input'));
        $objArr = (array) $obj;
        
        if (empty($objArr)) { $this->response(422, "error", "Nothing to add. Check json"); } 
        elseif (isset($obj->id) AND isset($obj->razonsocial) AND isset($obj->logo)) {
            $dirty = 0;
            $r = $this->db->updateFromMovil($obj->id, $obj->razonsocial, $obj->cuit, 
                    $obj->mediodepago, $obj->estado, $obj->logo, 
                    $obj->facebook,  $obj->twitter, $obj->instagram, 
                    $obj->palabrasclave,  $obj->web, $dirty, 
                    $obj->idcuenta, $obj->categorias);
        
            if ($r) { $this->response(200, "success", "record updated"); } 
            else { $this->response(422,"error","Duplicado"); }
        } else { $this->response(422, "error", "The property is not defined"); }
    }
    
    function putCategoriaXEmpresa() {
        $obj = json_decode(file_get_contents('php://input'));
        $objArr = (array) $obj;
        if (empty($objArr)) { $this->response(422, "error", "Nothing to add. Check json"); } 
        else if (isset($obj->idempresa) AND isset($obj->idcategoria)) {
            if ($this->db->updateCXE($obj->idempresa, $obj->idcategoria)) {
                $this->response(200, "success", "Record updated");
            } else { $this->response(304, "success", "Record not updated");}
        } else { $this->response(422, "error", "The property is not defined"); }
        exit;
    }
    
    function putImage() {
        $obj = json_decode(file_get_contents('php://input'));
        $r = $this->db->updateImage($obj->id, $obj->image);
        if ($r) {
            $this->response(200, "success", "update image");
        }
    }

    function proceesDelete() {
        if (isset($_GET['action']) && isset($_GET['id'])) {
            if ($_GET['action'] == self::API_ACTION) {
                $this->db->delete($_GET['id']);
                $this->response(204);
                exit;
            }
        }
        $this->response(400);
    }
}