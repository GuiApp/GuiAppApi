<?php
/**
 * Description of PaquetePremiumDB
 *
 * @author meza
 */
class PagoDB extends EntityDB{
    protected $mysqli;
    const TABLE = 'pagos';
    
    public function getList(){
        $query = "SELECT * FROM pagos;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
	
    public function getPaqueteById($idpaquete){
//        $query = "SELECT p.*, "
//            . "(CASE 1 WHEN c.fecinicio <= NOW() AND c.fecfin >= NOW() THEN 1 ELSE 0 END) AS comprado, "
//            . "IFNULL((CONCAT('Activo desde ', DATE_FORMAT(c.fecinicio, '%d/%m/%Y'), ' al ', DATE_FORMAT(c.fecfin, '%d/%m/%Y'))), '')As plazo "
//            . "FROM paquetes p "
//            . "LEFT JOIN paquetesxcuenta c ON c.idpaquete = p.id "
//            . "WHERE p.id=" . $idpaquete;
//        $result = $this->mysqli->query($query);
//        $entity = $result->fetch_all(MYSQLI_ASSOC);
//        $result->close();
//        return $entity;
        return true;
    }
	
    public function getFuncionesByIdPaquetes($idfuncion){
//        $query = "SELECT * FROM funcionesxpaquete "
//                . "WHERE idfuncion = " . $idfuncion;
//        $result = $this->mysqli->query($query);
//        $entity = $result->fetch_all(MYSQLI_ASSOC);
//        $result->close();
//        return $entity;
        return true;
    }

    public function getPaquetesByIdCuenta($idcuenta){
//        $query = "SELECT p.*, 
//                        (CASE 1 WHEN c.fecinicio <= NOW() AND c.fecfin >= NOW()THEN 1 ELSE 0 END) AS comprado, 
//                     IFNULL((CONCAT('Activo desde ', DATE_FORMAT(c.fecinicio, '%d/%m/%Y'), ' al ', DATE_FORMAT(c.fecfin, '%d/%m/%Y'))), '') As plazo FROM paquetes p 
//                LEFT JOIN paquetesxcuenta c ON c.idpaquete = p.id 
//                WHERE (p.activo = 1 OR c.idcuenta = '$idcuenta') AND p.registrado = 0";
////    var_dump($query);
//        $result = $this->mysqli->query($query);
//        $entity = $result->fetch_all(MYSQLI_ASSOC);
//        $result->close();
//        return $entity;
        return true;
    }
	
    public function insert($idcuenta='', $appleid=''){
        $query = "SELECT *
            FROM paquetes 
            WHERE appleid = '$appleid'";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        if(!isset($entity[0]['id']))
            return false;
        $idpaquete = $entity[0]['id'];
        $duracion = $entity[0]['duracion'];
        $costo = $entity[0]['costo'];
        
        $query = "INSERT INTO pagos 
                (idcuenta, idpaquete, fecultmodif)
                VALUES ('$idcuenta', $idpaquete, NOW());";
        
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        
        $query = "INSERT INTO paquetesxcuenta
                (idcuenta, idpaquete, fecinicio, 
                 fecfin, abonado)
            VALUES 
                ('$idcuenta', '$idpaquete', NOW(), 
                 DATE_ADD(NOW(), INTERVAL $duracion DAY), $costo);";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, 
            $idcuenta='', $idpaquete=-1) {
        $query = "UPDATE pagos SET 
                idcuenta='$$idcuenta', idpaquete='$idpaquete', fecultmodif = NOW()
            WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}