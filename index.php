<?php
    require_once "./controllers/EntityAPI.php";
    require_once "./controllers/EntityDB.php";
    require_once "./controllers/UsuarioAPI.php";
    require_once "./controllers/UsuarioDB.php";
    require_once "./controllers/RolAPI.php";
    require_once "./controllers/RolDB.php";
    require_once "./controllers/CuentaAPI.php";
    require_once "./controllers/CuentaDB.php";
    require_once "./controllers/EmpresaAPI.php";
    require_once "./controllers/EmpresaDB.php";
    require_once "./controllers/EventoAPI.php";
    require_once "./controllers/EventoDB.php";
    require_once "./controllers/EventoTipoAPI.php";
    require_once "./controllers/EventoTipoDB.php";
    require_once "./controllers/PublicacionAPI.php";
    require_once "./controllers/PublicacionDB.php";
    require_once "./controllers/SucursalAPI.php";
    require_once "./controllers/SucursalDB.php";
    require_once "./controllers/CategoriaAPI.php";
    require_once "./controllers/CategoriaDB.php";
    require_once "./controllers/ImagenAPI.php";
    require_once "./controllers/ImagenDB.php";
    require_once "./controllers/LocalizacionAPI.php";
    require_once "./controllers/LocalizacionDB.php";
    require_once "./controllers/PagoAPI.php";
    require_once "./controllers/PagoDB.php";
    require_once "./controllers/PaqueteAPI.php";
    require_once "./controllers/PaqueteDB.php";
    require_once "./controllers/PublicidadAPI.php";
    require_once "./controllers/PublicidadDB.php";
    require_once "./controllers/FuncionAPI.php";
    require_once "./controllers/FuncionDB.php";
    require_once "./controllers/SponsorAPI.php";
    require_once "./controllers/SponsorDB.php";
    
    header("Access-Control-Allow-Origin: *");
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS, PATCH');
    header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
    
    $action = $_GET['action'];
    $api = NULL;
    
    switch($action)
    {
        case UsuarioAPI::API_ACTION:
            $api = new UsuarioAPI();
            break;
        case RolAPI::API_ACTION:
            $api = new RolAPI();
            break;
        case CuentaAPI::API_ACTION:
            $api = new CuentaAPI();
            break;
        case EmpresaAPI::API_ACTION:
            $api = new EmpresaAPI();
            break;
        case EventoAPI::API_ACTION:
            $api = new EventoAPI();
            break;
        case PublicacionAPI::API_ACTION:
            $api = new PublicacionAPI();
            break;
        case SucursalAPI::API_ACTION:
            $api = new SucursalAPI();
            break;
        case CategoriaAPI::API_ACTION:
            $api = new CategoriaAPI();
            break;
        case EventoTipoAPI::API_ACTION:
            $api = new EventoTipoAPI();
            break;
        case ImagenAPI::API_ACTION:
            $api = new ImagenAPI();
            break;
        case LocalizacionAPI::API_ACTION:
            $api = new LocalizacionAPI();
            break;
        case PagoAPI::API_ACTION:
            $api = new PagoAPI();
            break;
        case PaqueteAPI::API_ACTION:
            $api = new PaqueteAPI();
            break;
        case PublicidadAPI::API_ACTION:
            $api = new PublicidadAPI();
            break;
        case FuncionAPI::API_ACTION:
            $api = new FuncionAPI();
            break;
        case SponsorAPI::API_ACTION:
            $api = new SponsorAPI();
            break;
        default:
            echo 'METODO NO SOPORTADO';
            break;
    }
    
    if($api != NULL)
        $api->API();
